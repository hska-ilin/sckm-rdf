---
title: "RDF-Modellierung: Good Practices" 
author: Andreas P. Schmidt
---

# Klassen vs. Instanzen # 

Im klassischen Datenmanagement beschäftigt man sich mit der Schema-Ebene zur Entwurfszeit und legt dann Klassen (bzw. Tabellen) und deren Attribute fest. Ebenso sind die Mechanismen zur Änderung vollkommen verschieden, so dass meist in der praktischen Anwendung nicht groß unterschieden werden muss. Im Gegensatz dazu ist bei RDF und ontologiebasierten Ansätzen bewusst die Unterscheidung zwischen beiden aufgehoben:

- Die Repräsentation, dass eine Ressource eine Klasse ist, geschieht durch einfache RDF-Tripel:
  
````ttl
    :Kompetenz a owl:Class .
````

- Ebenso die Aussage, dass eine Klasse eine Unterklasse einer anderen Klasse ist:

````ttl
    :Zeitungsartikel rdfs:subClassOf :Publikation .
````

- Will man eine Instanzbeziehung ausdrücken, dann ist das wiederum ein RDF.

````ttl
    :AwesomeNews a :Zeitungsartikel .
````

- All diese RDF-Tripel haben selbstverständlich nur dann Bedeutung, wenn das verarbeitende System das RDFS/OWL-Vokabular *versteht*, d.h. die Konzepte Klasse, Unterklassenbeziehung etc. kennt und interpretiert. Sonst sind es einfache Aussagen.

- Damit lassen sich auch Klassen durch Eigenschaften annotieren (dafür gibt es die AnnotationProperties). Diese können dazu benutzt werden, zur Laufzeit diejenigen Klassen zu filtern, die von jemandem eingeführt wurden.

````ttl
    :Zeitungsartikel :eingeführtVon :AS .
    :eingeführtVon   a              owl:AnnotationProperty .
````

- Theoretisch lassen sich damit natürlich auch Ketten von Instanzbeziehungen bauen, wie sie real durchaus vorkommen, z.B. ein Werk hat als Instanzen ein Buch in einer bestimmten Auflage/Ausgabe, diese wiederum als Instanzen das Buchexemplar, das in einer Bibliothek steht. **Allerdings** ist das nicht beliebig möglich. Außer OWL-Full schränken alle praktisch relevanten Untermengen die Verwendung Instanzketten so ein (so auch OWL-DL und -RL), dass dies nur möglich ist für z.B. ein Individum ist eine Instanz von Person; eine Person ist eine Instanz von Klasse, nicht jedoch für den oben geschilderten Zusammenhang zwischen Werken, Bücher und Buchexemplaren. Dort muss man sich entscheiden, wo man zur Klasse wechselt.

 > ### Woran erkennt man Individuen oder Klassen? 
 >  - Klassen sind immer Abstraktionen im Sinne von Mengen von Dingen, die man in ein gemeinsames Behältnis einteilt.
 >  - Sind im Text Plural-Formulierungen von Dingen (»Kompetenzen«, »Personen«), dann deutet dies auf Klassen hin
 >  - Unterklassenbeziehungen (die auf Klassen ja automatisch schließen lassen) deuten sich mit Formulierungen wie »Kompetenzen sind spezielle Themen«, »Publikationen können unterteilt werden in Zeitungsartikel, wissenschaftliche Artikel oder Monographien«, »Bücher sind immer auch Publikationen«


# Properties #

Wichtig ist die Erkenntnis, dass Properties in ontologiebasierten Ansätzen eigenständig sind und nicht an einer Klasse haften. Deshalb haben ihre Definitions- und Wertebereiche auch andere Aussagekraft; sie sagen etwas über die Individuen aus, bei denen sie verwendet werden.

## Definitions- und Wertebereich ##

 - Der Definitionsbereich `(rdfs:domain)` sichert zu, dass das Subjekt eines Statements, das die Property verwendet, Instanz der Klasse ist, die als Definitionsbereich angegeben wurde. Z.B. `:ist-autor-von rdfs:domain :Person` sichert zu, dass bei der Aussage `:Goethe :ist-autor-von :Faust` automatisch klar ist, dass `:Goethe` eine `:Person` ist. Es dient also dazu, die Individuen zu klassifizieren.
 - Der Wertebereich `rdfs:range` sicher analog dazu zu, dass das Objekt eines Statements von einer bestimmten Klasse sein muss. So könnte man mit  `ist-autor-von rdfs:range :Publikation` automatisch Instanzen wie `:Faust` klassifizieren.

Beides funktioniert auch mit mehreren Klassen als Domain und Range - dann wird die Instanzen *allen* Klassen, die angegeben wurden, zugewiesen. Beispiel 

````ttl
ist-autor-von rdfs:domain :Person .
ist-autor-von rdfs:domain :Urheber .
````

Dann sagt ist automatisch ausgesagt, dass `:Goethe` sowohl eine `:Person` als auch ein `:Urheber` ist.

> ### Woran erkennt man Aussagen über Definitions- und Wertebereiche? ###
> - Die Formulierungen sind ähnlich zu Klassen (da sie ja auch implizit klassifizieren, beziehen sich aber auf Beziehungen. Beispiel: *»Jemand, der Autor einer Publikation (oder: von etwas) ist, gilt als Schriftsteller«*. Dies lässt sich übersetzenin `:ist-autor-von rdfs:domain :Schriftsteller`. Die Aussage, dass es sich dabei um eine Publikation handeln musst, ist meist sprachlich gefordert, aber nicht mit Domain und Range zu spezifizieren. Hierfür braucht man dann Property-Restriktionen (s.u.).
> - Eine andere Möglichkeit ist: *»Wenn etwas veröffentlicht wurde, dann ist es eine Publikation«*. Das wird zu `:veröffentlicht rdfs:range :Publikation`. 

## Property-Hierarchies ##

Eine zweite Besonderheit stellen die Vererbungsbeziehungen auf Properties dar. Ihr Hauptanwendungsbereich dient der Überbrückung von Abstraktionsniveaus bei Aussagen.

So kann man danach fragen, ob jemand zu einer Publikation beigetragen hat, ohne unterscheiden zu müssen, ob jemand als Autor, Übersetzer oder Herausgeber gilt. Für andere Nutzungsszenarien mag allerdings die Differenzierung wichtig sein. Jede Anwendung kann auf ihre Abstraktionsebene zugreifen; andere können allgemeiner oder spezieller unterwegs sein.

Wesentliches Konstrukt ist hier die `rdfs:subPropertyOf`-Beziehung, die automatisch die beiden Beteiligten Ressourcen zu Properties macht.

> ### Woran erkennt man Aussagen über Property-Hierarchies? ###
> - Oft sind die Aussagen (wenn präzise) sprachlich eher sperrig: *»Autorsein ist eine spezielle Form des Beitragens«*.
> - Allerdings kann man es manchmal auch natürlicher als *»Wenn jemand ein Autor ist, dann ist er immer auch ein Beitragender«*. Das lässt sich leicht verwechseln mit Klassenhierarchien und kann - für sich allein genommen - natürlich auch so modelliert werden. Allerdings ergibt sich aus dem Zusammenhang (man hat die Autorschaft bereits als Beziehung ausgedrückt), dass eine Klasse Autor und eine Klasse Beitragendender nur daneben stehen, so dass eine praktikablere Lösungsmöglichkeit über die Aussage `:ist-autor-von rdfs:subPropertyOf :ist-beitragender-von`.
> - Analog können auch Formulierungen wie *»Wurde etwas veröffentlicht, dann ist es auch (automatisch, zwingend, immer) zugänglich«* auf eine Property-Hierarchie als Lösungsmöglichkeit hindeuten.


## Property-Eigenschaften ##

Für Properties können auch (mathematische) Relationseigenschaften angegeben werden, die ja über einfaches `:ist-befreundet-mit a owl:SymmetricProperty` zugewiesen werden könnne.

> ### Woran erkennt man Aussagen über Property-Eigenschaften? ###
> - Für Transitivität muss man meist der Präzision willen recht formale Aussagen machen: *Wenn A in B liegt und B in C, dann liegt auch A in C*. Dies beschreibt die Transitivität von `liegt-in`: `:liegt-in a owl:TransitiveProperty`. 
> - Ansonsten läuft es meist auf Ordnungen hinaus (ist größer/wichtiger/besser als), die sich als transitiv modellieren lassen. Da ist die *Ordnung* der eigentliche Hinweis. Hier lässt sich noch unterscheiden, ob es sich um reflexive Ordnungen handelt (ist größer oder gleich), was sich für Anfragen wie  *»mindestens auf dem Niveau«* eignet: `:besser-als a owl:TransitiveProperty, owl:ReflexiveProperty`, wobei `besser-als` entgegen der mathematischen Fachsprache als reflexiv angesehen wird und nicht als `besser-oder-gleich` geschrieben würde.
> - Symmetrie lässt sich durch *»gilt immer in beide Richtung«* o.ä. ausdrücken.
> - Interessante Fälle sind durch *FunctionalProperties* möglich. Hinweise für eine funktionale Property sind *»Zu jedem Modul gibt es (genau/höchstens) einen Verantwortlichen«* oder *»Zu jedem Modul gibt es einen eindeutigen Verantwortlichen«* (das genau lässt sich ohne Restrictions nicht abbilden): `:hat-verantwortlichen a owl:FunctionalProperty`.
> - In umgekehrte Richtung können Formulierungen wie *»Es kann immer nur einen Zuständigen für eine Aufgabe geben«* auf die InverseFunctional-Eigenschaft der Beziehung `:zuständig-für` hindeuten: `:zuständig-für a owl:InverseFunctionalProperty`

