---
title: RDF, SPARQL und OWL
author: Andreas P. Schmidt
titlepage: true
titlepage-rule-color: bf2e11
titlepage-rule-height: 16
titlepage-color: eeeeee
titlepage-text-color: "000000"
fontfamily: FiraSans
fontfamilyoptions:
 - "default"
---
# RDF

Die Grundlage von [RDF](https://www.w3.org/RDF/) bilden Ressourcen und Tripel (sog. _Statements_). 
*Ressourcen* sind (global eindeutig) durch
URIs (in den neuen Versionen sog. IRIs, die eine Verallgemeinerung von URIs um weitere Unicode-Zeichen darstellen) 
identifizierbare Entitäten. Wenn eine Ressource dieselbe URI trägt, dann ist die Ressource auch identisch. 
URIs können unterschiedliche Form annehmen. Die häufigsten sind _http-URL_ oder _URN_.

*Statements* sind *Tripel* aus Subjekt, Prädikat und Objekt, wobei Subjekt und Prädikat jeweils Ressourcen sein 
müssen; Objekte können Ressourcen oder *Literale* sein. Sie entsprechen atomaren Datentypen wie Zeichenketten
und werden oft mit Hilfe von XML-Schema-Datentypen typisiert.  

## Turtle-Notation

Die [Turtle-Notation](https://www.w3.org/TR/turtle/) ist eine kompakte Notation von RDF-Tripeln, die für
die manuelle Erstellung durch menschliche Nutzer gedacht ist.

Tripel lassen sich wie folgt darstellen:

````ttl
<http://km.teaching.ilin.eu/apschmidt> <http://km.teaching.ilin.eu/teaches> <http://km.teaching.ilin.eu/sckm> . 
````

URIs sind meist unhandlich blähen die Tripel-Notation unnötig auf. Präfixe kürzen die URI-Schreibweise ab 
und funktionieren ähnlich wie XML-Namensräume: 
es wird ein Namensraum-Präfix dem Beginn einer URI zugeordnet. Im weiteren kann das dann als
`prefix:fragment` notiert werden. Präfix plus Fragment ergeben dann die Gesamt-URI.

In Turtle wird das wie folgt repräsentiert:

````ttl
PREFIX : <http://km.teaching.ilin.eu/> 
PREFIX skos:  <http://www.w3.org/2004/02/skos/core#>
:apschmidt :teaches :sckm .
:apschmidt :teaches :esm .

:sckm      skos:prefLabel "Social Collaboration & Knowledge Management" .
:sckm      skos:altLabel  "Social Collab" .
:sckm      skos:hiddenLabel "Wissensmanagement & -modellierung " .
:sckm      :sws  "4"^^xsd:int .
````

![Trennung von sprachlichen Symbolen und Konzepten mittels SKOS](SCKM_Tutorial_1.png)

Im Beispiel wurde ein weiteres Vokabular, das sog. SKOS-Vokabular eingesetzt, um
den URIs lesbare Bezeichner zuordnen zu können. Das könnte man auch mit `rdfs:label`,
was häufiger verwendet wird, aber SKOS lässt hier zu, besser zu differenzieren:

* `skos:prefLabel` für den bevorzugten oder "offiziellen" Bezeichner
* `skos:altLabel` für Synonyme 
* `skos:hiddenLabel` für fehlerhafte oder veraltete Schreibweisen

Wie man an SKOS erkennen kann, kann man fremde Vokabulare einfach dadurch verwenden, 
dass man die jeweilige URI benutzt.

Man kann auch Mehrsprachigkeit abbilden, indem man statt `"String"` die Schreibweise
`"String"@lang` benutzt, wobei `lang` für die zweistelligen ISO-Sprachcodes steht (wie z.B. `de`  oder `en`).

Zeichenketten lassen sich auch näher typisieren, indem man XML-Schema verwendet, z.B. Datumswerte 
`"2018-12-19T15:40:00"^^xsd:dateTime`. Der zugehörige Namensraumpräfix wäre 
`PREFIX xsd: <http://www.w3.org/2001/XMLSchema>`.

Im obigen Beispiel gab es viele Wiederholungen durch die Zuordnung zu denselben Ressource. 
Für längere Faktenlisten existieren daher abkürzende Schreibweisen:

### Mehrere Statements mit demselben Subjekt

````ttl
:sckm      skos:prefLabel "Social Collaboration & Knowledge Management" ;
           skos:altLabel  "Social Collab" ;
           skos:hiddenLabel "Wissensmanagement & -modellierung " ; 
           :sws "4"^^xsd:int
````

### Mehrere Statements mit demselben Subjekt *und* Prädikat

````ttl
PREFIX : <http://km.teaching.ilin.eu/>
:sckm :covers :RDF, :OWL, :KnowledgeCafe, :KM.
````

## Anonyme Ressourcen

Manchmal sind mehrstellige Beziehungen erforderlich, z.B. wenn man ausdrücken will, dass eine Person eine bestimmte Kompetenz in einem Thema auf einer bestimmten Stufe hat. Dann lässt sich das allgemein mit Zwischenknoten lösen, die keine wirkliche semantische Bedeutung tragen außer als Behälter für die mehrstellige Beziehungsinstanz. Anstatt künstlich URIs zu erzeugen, existiert die Möglichkeit, anonyme Ressourcen zu nutzen:

````ttl
:alice :has-competency [ :competency :RDF ;  :level :Intermediate  ] .
````

Das ist gleichbedeutend zu:

````ttl
:alice :has-competency :c1 .
:c1    :competency     :RDF ;
       :level          :Intermediate
````

![Potentiell anonyme Zwischenknoten](SCKM_Tutorial_2.png)

# Ontologien

Ontologien dienen in der RDF-Welt nicht dazu, Zusicherungen über die Daten
zu repräsentieren. Sie dienen vielmehr dazu, die vorhandenen Daten automatisiert 
zu vervollständigen mit zusätzlichen Fakten. In klassischen Datenbanksystemen
gilt ein _schema-first_-Ansatz, d.h. zuerst wird das Schema definiert und dann
die Daten entsprechend dem Schema hinzugefügt. Das Schema verletzende Daten sind
nicht zulässig, so dass das Schema eine Konsistenzsicherungsfunktion hat.

Im Gegensatz dazu gilt bei RDF _data first_, d.h. die Fakten in RDF-Form können 
unabhängig vom Schema existieren. Das Hinzufügen einer Ontologie führt dazu,
dass durch sog. *Reasoning* (logisches Schlussfolgern) aus den vorhandenen Fakten
neue geschlossen werden können. Es hat dieselbe Wirkung wie das Hinzufügen von
neuen Fakten.

Ontologien werden selbst als RDF repräsentiert. So lassen sich auch Anfragen auf
Ontologieebene (Metaebene) mit Anfragen auf Instanzebene mischen. Ob die Bedeutung 
der Ontologiekonstrukt erkannt wird, 
hängt davon ab, ob das Zielsystem (z.B. der entsprechende Triple-Store oder Reasoner)
die Sprachkonstrukte versteht, also die Ontologiesprache unterstützt. Sonst ist das
Ontologievokabular nichts anderes als jedes beliebige andere Vokabular. Ein Reasoning
ist dann nicht möglich.

## OWL

OWL hat sich nach mehreren Vorläufern (u.a. RDF Schema) als Standard-Ontologiesprache
herausgebildet. Sie existiert in mehreren Untervarianten, die sich auf bestimmte
Konstrukte beschränken, damit die Ontologien leichter auf bestimmten Technologien
umgesetzt werden können, so z.B. *OWL-RL* für die Ausführung auf regelbasierten Systemen
oder *OWL-QL* auf anfragebasierten (Datenbank-)Systemen.

Die Historie der Sprache merkt man noch an den unterschiedlichen Namensräumen, die in
OWL-Ontologien verwendet werden.

## Klassen

Eine Instanz wird durch folgendes Tripel einer Klasse zugeordnet:

````ttl
:sckm rdf:type :MasterCourse .
:esm  a :BacherlorCourse .
````

Abkürzend kann auch für `rdf:type` die Schreibweise `a` eingesetzt werden (kommt von _is-a_ als 
übliche Bezeichnung für Instanzbeziehungen).

Eine Klasse wird wie folgt explizit definiert:

````ttl
:Course a owl:Class .
````

Auf Klassen können auch Vererbungshierarchien definiert werden, wobei die Mehrfachvererbung 
zugelassen ist.

````ttl
:MasterCourse rdfs:subClassOf :Course .
:BachelorCourse rdfs:subClassOf :Course .
````

![Subklassenbeziehungen](SCKM_Tutorial_3.png)

Es lassen sich auch _Aufzählungsklassen_, wie man sie aus modernen objektorientierten
Sprachen kennt, definieren, bei denen die Klasse nur aus genau den Instanzen besteht, 
die angegeben sind:

`:Level owl:oneOf (:Beginner :Intermediate :Expert) .`

### Automatisierte Zuordnung von Instanzen zu Klassen

#### Klassenvereinigung

Eine Klasse `:neueKlasse` lässt sich auf der Basis von zwei Klassen `:klasse1` und `:klasse2` definieren. 
Damit ist die Instanzmenge von `:neueKlasse` die Vereinigung der Instanzmengen von `:klasse1` und `:klasse2`.

````ttl
:neueKlasse owl:unionOf (:klasse1 :klasse2) .
````

#### Klassendurchschnitt

Analog dazu lässt sich `:neueKlasse2` als der gemeinsame Menge der Instanzmengen von `:klasse1` und `:klasse2` definieren.


````ttl
:neueKlasse2 owl:intersectionOf (:klasse1 :klasse2) .
````

#### Property Restrictions

Klassen lassen sich auch auf der Basis von Property-Werten definieren. So können wir
`:SCKMLecturer` als diejenigen Instanzen definieren, die über eine Property 
`:teaches` verfügen und auf den Wert `:sckm` verweisen, wobei sowohl
Literale als auch Ressourcen zugelassen sind.

````ttl
:SCKMLecturer a owl:Restriction ;
    owl:onProperty :teaches ;
    owl:hasValue   :sckm .
````

Man kann das auch verallgemeinern auf Klassen von Werten. Will man aussagen, dass
`:LecturerOnlyInMaster` diejenigen Instanzen umfasst, die über eine Property 
`:teaches` verfügen und _nur_ auf Werte verweist, die Instanz der Klasse  `:MasterCourse` sind.

````ttl
:LecturerOnlyInMaster a owl:Restriction ;
    owl:onProperty :teaches ;
    owl:allValuesFrom :MasterCourse .
````

Eine Variante davon ist, dass man `:LecturerInMaster` auch so definieren kann, dass sie die Instanzen umfasst, 
die über eine Property `:teaches` verfügen und *auf mindestens einen Wert* verweist, der Instanz der Klasse  `:MasterCourse` ist.

````ttl
:LecturerInMaster a owl:Restriction ;
    owl:onProperty :teaches ;
    owl:someValuesFrom :MasterCourse .
````

### Disjunktheit

Da bei RDF eine offene Welt gilt (was unbekannt ist, ist nicht automatisch falsch, sondern unbekannt), sind auch Negativaussagen nützlich, z.B. die häufige Disjunktheit zwischen Klassen. Sie vermeidet auch semantische Widersprüche (die dadurch als Inkonsistenzen erkannt werden können)

## Properties

Neben Klassen sind in OWL (wie auch bereits in RDF) sind Properties ebenso 
_first class citizens*. Im Gegensatz zu objektorientierten Ansätzen, wo Attribute
immer an eine Klasse gebunden sind, existieren sie hier unabhängig davon. 
Ebenso können sie analog zu Klassen eine Hierarchie bilden. Dadurch lassen sich 
auch auf Property-Ebene Abstraktionen bilden.

Die Gleichwertigkeit von Klassen und Properties führt auch zu Freiheitsgraden in
der Modellierung, je nachdem, ob man die Semantik in die Klassen (_class centric_) oder
in die Properties legt (_property centric_). Gerade letztere Ansatz hat sich in
manchen Anwendungsfällen als natürlicher herausgestellt.

Properties in OWL gibt es in zwei Varianten: als `owl:ObjectProperty` (literalwertig) 
oder als `owl:DatatypeProperty`

````ttl
:teaches a owl:ObjectProperty .
````

Will man Wissen darüber repräsentieren, dass Properties auf bestimmte Werte- oder
Definitionsbereiche hindeuten, dann kann man `domain` (Definitionsbereich) und
`range` Wertebereich festlegen:

````ttl
:teaches 
    a owl:ObjectProperty ;
    rdfs:domain :Person ;
    rdfs:range  :Course .
````

Dabei ist das keine Konsistenzbedingung, sondern ein Hinweis für das Reasoning.

Gilt `:apschmidt :teaches :sckm`, dann kann geschlossen werden, dass
`:sckm` eine Instanz von `:Person` ist und `:sckm` von `:Course`.

Mehrere Angaben für `domain` oder `range` haben dabei die Bedeutung, dass der gesamte
Definitionsbereich die Schnittmenge der einzelnen Angaben ist.

Also:

````ttl 
:beispielProperty 
    rdfs:domain :klasse1 ;
    rdfs:domain :klasse3 ;
    rdfs:range  :klasse2 .
````

Damit ist der Definitionsbereich von `:beispielProperty` nun die Menge aller
Instanzen, die Instanz von `:klasse1` und `:klasse3` sind. 

*Propertyhierarchien* werden wie folgt aufgebaut:

````ttl
:knows a owl:ObjectProperty 
    rdfs:range :Person .
:knows-student rdfs:subPropertyOf :knows.
    rdfs:range :Student .
````

Dabei werden `domain` und `range` vererbt. Auf der Kind-Property können
dann zusätzliche Werte- und Definitionsbereiche angegeben werden, um sie
einzuschränken (Wirkung wie oben über die Schnittmenge aller gültigen Aussagen). Hier also sind der Wertebereich nur Instanzen der Klasse Student (weil Subklasse).

![Property-Hierarchien und Definitions-/Wertebereiche](SCKM_Tutorial_4.png)

### Beziehungseigenschaften

OWL definiert einige häufig vorkommende und mathematisch formalisierbare Typen:

-  `owl:ReflexiveProperty` definiert, dass eine Instanz auch _immer_ mit sich selbst in Beziehung steht (reflexiv)
-  `owl:IrreflixiveProperty` definiert, dass eine Instanz _nie_ mit sich selbst in Beziehung steht (irreflexiv)
-  `owl:SymmetricProperty` definiert für eine Property `:p`, dann wenn `:a :p :b` auch `:b :p :a` gilt (symmetrisch)
-  `owl:TransitiveProperty` definiert für eine Property `:p`, dann wenn `:a :p :b` und `:b :p :c` auch `:a :p :c` gilt (transitiv)
-  `owl:FunctionalProperty` legt für eine Property `:p` fest, dass es _von_ einer Instanz nur einmal die Eigenschaft gibt, 
    also dass wenn `:a :p :b` und `:a :p :c` dann muss `:b` = `:c` sein
-  `owl:InverseFunctionalProperty` legt für eine Property `:p` fest, dass es _zu_ einer Instanz nur einmal die Eigenschaft gibt, 
    also dass wenn `:b :p :a` und `:c :p :a` dann muss `:b` = `:c` sein

Beispiele sind strikte und nicht-strikte Ordnungen, wie z.B. *besser als*. Hier lässt sich die nicht-strikte Version der
Ordnung als reflexiv definieren.

````ttl
:better-than-or-equal 
    a owl:TransitiveProperty ;
    a owl:ReflexiveProperty .

:better-than a owl:TransitiveProperty .
:Intermediate :better-than :Beginner .
:Expert :better-than :Intermediate .

:Intermediate :better-than-or-equal :Beginner .
:Expert :better-than-than-or-equal :Intermediate .

````

![Reflexivität und Transitivität von Beziehungen](SCKM_Tutorial_5.png)

Außerdem lässt sich zwischen Properties festlegen, dass sie äquivalent sind `:p1 owl:equivalentProperty :p2` und dass eine
Property `:p2` die Inverse zu einer Property `:p1` ist: `:p2 owl:inverseOf :p1`.

````ttl
:is-known-by owl:inverseOf :knows .
````

# SPARQL

SPARQL hat sich als Standard-Anfragesprache für RDF-Daten etabliert. Dabei ist zu beachten, dass SPARQL die Interpretation einer bestimmten Ontologiesprache wie OWL nicht voraussetzt. Daher gibt es SPARQL-Endpoints, die nur _plain RDF_ verstehen, andere, die OWL in einer bestimmten Ausprägung verstehen und wieder andere, die eine andere Ontologiesprache unterstützen. Dies führt natürlich zu unterschiedlichen Ergebnissen.

Im folgenden unterstellen wir die OWL-RL-Semantik, wie sie von z.B. GraphDB unterstützt wird.

## Grundstruktur

````rq
PREFIX ...
SELECT ?x ?y 
WHERE {

}
````

Die SELECT-Klausel (Projekt) legt die Variablen fest, die für die tabellarische Ergebnisstruktur (Tupelmenge) benutzt werden. Variables werden mit `?` eingeleitet.

Die WHERE-Klausel besteht aus Triple-Patterns, die eine oder mehrere Bestandteile des Tripels durch Variablen ersetzen. Sie werden in Turtle-Notation notiert

Mehrere Triple-Patterns werden konjunktiv verknüpft, was die Wirkung eines Joins hat. Dabei wirken gemeinsame Variables als INNER-JOIN-Bedingungen.

````rq
PREFIX : <http://km.teaching.ilin.eu/>
PREFIX skos:  <http://www.w3.org/2004/02/skos/core#>
SELECT ?x ?l
WHERE {
    ?x :teaches ?v .
    ?v skos:prefLabel ?l .
}
````

Das Ergebnis wäre hier

| ?x         | ?l                                          |
|------------|----------------------------------------------|
| :apschmidt | "Enterprise Social Media"                    |
| :apschmidt | "Social Collaboration & Knowledge Management |

Für die Abkürzung von Ketten von Patterns existiert eine Kurznotation, die auf die Zwischenvariablen verzichtet.

````rq
PREFIX : <http://km.teaching.ilin.eu/>
PREFIX skos:  <http://www.w3.org/2004/02/skos/core#>
SELECT ?x ?l
WHERE {
    ?x :teaches/skos:prefLabel ?l .
}
````

Will man bei mehreren Pattern nicht voraussetzen, dass alle zutreffen, dann könnnen Pattern auch als `OPTIONAL` deklariert werden. Das wirkt dann wie ein OUTER-JOIN aus SQL, d.h. die fehlenden Variablen werden mit `NULL` belegt. 

````rq
PREFIX : <http://km.teaching.ilin.eu/>
PREFIX skos:  <http://www.w3.org/2004/02/skos/core#>
SELECT ?x ?v ?l
WHERE {
    ?x :teaches ?v .
    OPTIONAL { ?v skos:prefLabel ?l . }
}
````

Das Ergebnis wäre hier

| ?x         | ?v    | ?l                                           |
|------------|-------|----------------------------------------------|
| :apschmidt | :esm  | "Enterprise Social Media"                    |
| :apschmidt | :sckm | "Social Collaboration & Knowledge Management |
| :apschmidt | :mod  | _NULL_                                       |

## Disjunktionen (ODER) 

Oder-Verknüpfungen lassen sich einfach über `UNION` realisieren:

````rq
PREFIX : <http://km.teaching.ilin.eu/>
PREFIX skos:  <http://www.w3.org/2004/02/skos/core#>
SELECT DISTINCT ?p ?y
WHERE {
    {
        ?p :has-competency ?c . 
        ?c :competency ?y . 
        ?c :level :Expert .
    } UNION {
        ?p :has-competency ?c . 
        ?c :competency ?y . 
        ?c :level :Intermediate .
    }
}
````

Mit den Daten

````ttl
:alice :has-competency [ :competency :RDF ;  :level :Intermediate  ] .
:alice :joined :sckm .
:bob   :joined :esm .
:carol :joined :sckm ; 
       :has-competency [ :competency :KM ; :level :Expert ] .
````

ergibt dies:

| ?p         | ?y    |
|------------|-------|
| :carol     | :KM   |
| :alice     | :RDF  | 

Analog zu `UNION` kann auch eine Mengendifferenz per `MINUS` gebildet werden. Dies kann zur Umsetzung von Negation eingesetzt werden.

## Filter

Neben den Triple-Patterns gibt es zusätzlich noch nachgeschaltete Filter, die eine Ergebnismenge nach bestimmten Kriterien weiter verfeinern. Hierbei steht eine ganze Reihe von Funktionen zur Verfügung. So können auf Datentypen Vergleiche durchgeführt werden

````rq
PREFIX : <http://km.teaching.ilin.eu/>
PREFIX skos:  <http://www.w3.org/2004/02/skos/core#>
SELECT ?p ?l
WHERE {
    ?p :teaches ?y .
    ?y skos:prefLabel ?l .
    ?y :sws ?s .
    FILTER(?s >= 4)
}
````

Das Ergebnis wäre hier

| ?p         | ?l                                            |
|------------|-----------------------------------------------|
| :apschmidt | "Social Collaboration & Knowledge Management" |

oder ein SQL `LIKE` Operator mit regulären Ausdrücken nachgeahmt werden

````rq
PREFIX : <http://km.teaching.ilin.eu/>
PREFIX skos:  <http://www.w3.org/2004/02/skos/core#>
SELECT ?p ?l
WHERE {
    ?p :teaches ?y .
    ?y skos:prefLabel ?l .
    FILTER( regex(?l, "Social") )
}
````

| ?x         | ?l                                            |
|------------|-----------------------------------------------|
| :apschmidt | "Enterprise Social Media"                     |
| :apschmidt | "Social Collaboration & Knowledge Management" |


Ebenso kann über `EXISTS` oder `NOT EXISTS` teilweise eleganter eine Prüfung vorgenommen werden. Mit den folgenden Daten:

````ttl
:alice :has-competency [ :competency :RDF ;  :level :Intermediate  ] .
:alice :joined :sckm .
:bob   :joined :esm .
:carol :joined :sckm ; 
       :has-competency [ :competency :KM ; :level :Expert ] .
````
ergibt sich dann:

````rq
PREFIX : <http://km.teaching.ilin.eu/>
PREFIX skos:  <http://www.w3.org/2004/02/skos/core#>
SELECT ?p
WHERE {
    ?p :joined ?y .
    FILTER( NOT EXISTS { ?p :has-competency/:competency :RDF } )
}
````

| ?x         |
|------------|
| :carol     |  

## Weitere Anfragetypen

Neben der reinen Abfrage sind noch weitere Anfragetypen möglich. Dazu gehören 

- `DESCRIBE`. Dabei werden alle bekannten Fakten über die Ressource (mit ihr als Subjekt) zurückgegeben in der Form einer RDF-Serialisierung, inkl. der damit verbundenen Ressourcen. Allerdings ist der genaue Umfang des Ergebnisses implementierungsabhängig, da im Standard nicht festgelegt.

````rq
PREFIX : <http://km.teaching.ilin.eu/>
PREFIX skos:  <http://www.w3.org/2004/02/skos/core#>
DESCRIBE ?p
WHERE {
    ?p :joined ?y .
    FILTER( NOT EXISTS { ?p :has-competence/:competency :RDF } )
}
````

- `ASK` für die Rückantwort, ob eine Lösung gefunden werden konnte (nicht: ob diese möglich ist)

- `CONSTRUCT` für die Konstruktion eines Graphen als Ergebnis. Dies auch noch durch `INSERT` dazu genutzt werden, dass die Ergebnisse in das Repository geschrieben werden. Dadurch werden weitere "Vervollständigungsregeln" möglich, die über die Ontologiemechanismen hinausgehen und deren Ergebnis einfach materialisiert wird. Durch die Nutzung von Subgraphen lässt sich gezielt das Ergebnis einer Anfrage auch ersetzen und so periodisch aktualisieren.

# Anwendungsfälle

## Datenintegration aus unterschiedlichen heterogenen Datenquellen 

Daten aus unterschiedlichen autonomen und heterogenen Systemen zusammenzuführen ist ein in vielen Kontexten 
auftretendes Problem, das auch mit vielen unterschiedlichen Mitteln in Angriff genommen wird. Besonderes 
Interesse bekommt dieses Thema insbesondere unter dem Aspekt einer intensiveren Nutzung der Daten für 
weitergehende Analysezwecke oder der Generierung von Trainingsdaten für KI-Methoden. Klassische Methoden
(wie z.B. Data-Warehousing-Ansätze) setzen voraus, dass zunächst ein integriertes Schema entwickelt wird, 
für das dann Übersetzungsregeln definiert werden, die die vorhandenen Datenstrukturen in dieses neue 
Schema übersetzen. Dies setzt voraus, dass das Zielschema (oder zumindest die Anforderungen daran) im voraus
bekannt sind und bedeutet für einigermaßen komplexe Schemata einen erheblichen Aufwand. 

RDF-basierte (oder ontologiebasierte) Integration geht einen etwas anderen Weg und betrachtet das Problem
als ein **Wissensproblem**. Schemaintegration ist deshalb kompliziert, weil (a) die Datenschemata auf einem
zu niederigen konzeptuellen Abstraktionsniveau beschrieben sind und (b) weil das Domänenwissen, das für
die Zusammenführung benötigt wird, nicht in maschinenauswertbarer Form zur Verfügung steht. Die Ansätze konzentrieren   
sich daher nicht auf die Herstellung eines bestimmten Zielschemas (das kann dabei als Nebenprodukt herauskommen),
sondern auf die Wissensmodellierung. Man ergänzt die Daten um zusätzliches Wissen über Strukturen und Zusammenhänge,
so dass Maschinen fehlende Informationen ergänzen und Abstraktionsebenen überbrücken können.

Welche typischen Integrationsprobleme werden so gelöst?

- **Äquivalenz auf Schemaebene** Es können sowohl für Properties als auch für Klassen Äquivalenzen formuliert werden. Als
  schwächere Variante kann auch eine Subklassen- oder Subproperty-Beziehung eingesetzt werden. Dadurch werden bidrektionale
  Abbildungen möglich, d.h. aus den bisherigen Schemata kann auf die jeweils anderen Daten zugegriffen werden.
- **Äquivalenz auf Instanzebene** Es kann für Instanzen eine *sameAs*-Beziehung festgelegt werden.
- **Nutzung unterschiedlicher Abstraktionsebenen** durch Subproperty- und Subklassenstrukturen.
- **Fehlende Klassifikation und Typisierung** durch Property-Definitionen (Wertebereich und Definitionsbereich) oder
  Property-Restriktionen (Klassifikation auf der Basis von Werten bestimmter Properties).

Weitergehende Transformationen können von Regelmechanismen ausgeführt werden. Ein einfacher, aber sehr mächtiger
Ansatz sind SPARQL CONSTRUCT/INSERT-Anfragen, die auf der Basis vorhandener Daten zusätzliche Fakten generieren.
Dabei können die Ergebnisse in Subgraphen gepackt werden, die sich auch bei Aktualisierungen des zugrundeliegenden 
Datenbestandes kontrolliert mit aktualisieren lassen.