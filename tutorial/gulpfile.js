const gulp = require('gulp');
const pandoc = require('gulp-pandoc');
const pdf = require('gulp-pandoc-pdf');
 
gulp.task('html', (done) => {
  gulp.src('*.md')
    .pipe(pandoc({
      from: 'markdown',
      to: 'html5',
      ext: '.html',
      args: ['--template=templates/GitHub.html5', '--self-contained']
    }))
    .pipe(gulp.dest('.'));
    done();
});

gulp.task('pdf', (done) => {
    gulp.src('*.md')
        .pipe(pdf({
            pdfDir: '.',
            args: ['--template=templates/ilin.latex', '--listings']    
        })) 
        .pipe(gulp.dest('.'));
    done();
});