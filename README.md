## Getting started ##

GraphDB ist in Repositories organisiert, die man in klassischen relationalen System mit Datenbanken vergleichen kann. Innerhalb von Repositories können noch sog. 
Named Graphs (Teilgraphen) angelegt werden. Einer dieser Teilgraphen ist der “default graph”, der bei Anfragen automatisch auch alle Teilgraphen beinhaltet. 
Links unter “Setup > Repositories”, beim jeweiligen Repository auf das Connect-Icon links klicken) sich das Repository wechseln; eine schnelle Umschaltung 
ist auch rechts oben möglich. Momentan existiert das Repository für das Vorlesungsbeispiel auf den Folien und an der Tafel (sckm-ws1819), das SPARQL-Beispiel für das Übungsblatt (sckm-uebungsblatt), das in den Folien verlinkte Cambridge-Semantics-Tutorial (cambridge-semantics) sowie das Repository für das [neue Tutorial](https://gitlab.com/hska-ilin/sckm-rdf/tutorial) (*sckm*). Auf der linken Seite könnt Ihr unter “Explore” die Statements einsehen; 
unter SPARQL kann man dann Anfragen testen.

Ihr könnt eigene Daten in das Repository _sckm-ws1819-sandbox_ importieren (in unterschiedlichen Formaten). Dabei könnt ihr unterschiedliche Teilgraphen anlegen 
und so eigene gewisse Separation hinbekommen. Wie gesagt, richte ich gerne individuelle Nutzer ein.

Für das *Tutorial* *(sckm)* sind im Tutorial die Beispielanfragen enthalten.

### Anfragen im Vorlesungsbeispiel (sckm-ws1819) ###

````sparql
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>	
PREFIX : <http://km.teaching.ilin.eu/#>
SELECT * 
WHERE { 
	?s rdf:type :Kompetenz .
} 
````

````sparql
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX : <http://km.teaching.ilin.eu/#>
SELECT * 
WHERE { 
	?s :hat-kompetenz/:kompetenz :Sozialkompetenz .
} 
````

### Anfragen im Übungsblatt-Repository (sckm-uebungsblatt) ### 

````sparql
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX : <http://km.teaching.ilin.eu/#>

SELECT ?p
WHERE { 
	:carol :veröffentlicht ?p .
}
````

### Namensräume ###

Falls ihr die OWL-Namensräume braucht, hier für einfaches Copy & Paste:

````
PREFIX   rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
PREFIX   rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
PREFIX   owl:  <http://www.w3.org/2002/07/owl#> 
PREFIX   xsd:  <http://www.w3.org/2001/XMLSchema#> 
PREFIX   skos: <http://www.w3.org/2004/02/skos/core#>
````

### Weitere Infos ###

Wer sich GraphDB auf dem eigenen Rechner installieren will, kann dies tun. Sofern Docker unterstützt wird (Win 10 Pro, Linux, MacOS), ist dies die einfachste Möglichkeit: https://github.com/manasRK/graphdb-free


## Lizenz ##

Die Daten und Dokumente sind unter Creativ Commons CC-BY-4.0 lizensiert (siehe auch LICENSE.md). Die Materialien wurden erstellt für die Vorlesung *Social Collaboration & Knowledge Management* an der [Hochschule Karlsruhe, Institut für Lernen & Innovation in Netzwerken (ILIN)](https://ilin.eu).